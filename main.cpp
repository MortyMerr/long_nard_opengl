#include <glut.h>
#include "GraphicUtils.hpp"
#include "defs.hpp"
#include <time.h>

int main(int argc, char * argv[])
{
  //srand(time(NULL)); //for random dices
  //all functions with "glut" is from OpenGl
  glutInit(&argc, argv);			
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
  glutInitWindowSize(windowWidth - 20, windowHeight);
  //glutInitWindowPosition(0, 0); //u can change window position
  glutCreateWindow("Nards long");
  glClearColor(0.5, 0.5, 0.5, 0.5);	
  glutMouseFunc(mouseButton);
  glutMotionFunc(mouseMove);
  glutDisplayFunc(draw);		
  glutReshapeFunc(reshape); 
  glutMainLoop(); 
  return 0;
}