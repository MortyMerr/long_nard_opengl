#ifndef NARD_CHECKER_HPP
#define NARD_CHECKER_HPP

#include "Printable.hpp"
#include "Point.hpp"

//TODO checker class. Don't know, maybe it's wrong translation to '�����'
class Checker :
  public Printable
{
public:
  enum class Color {black, white, no};

  Checker(Color black, Point&& center);

  void print() const override;

  Color getColor() const;
  void move(size_t x, size_t y);
  void move(Point&& point);
public://FIXME
  Point center_;
  double r_;
  Color color_;
};

#endif // !NARD_CHECKER_HPP