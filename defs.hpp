#ifndef NARD_DEFS_HPP
#define NARD_DEFS_HPP
//TOUSER here you can configure width and height, �� ��� �� �����. unsafe

#define housesN 12
#define yOfLowLayerOfHouses 50
#define yOfUpLayerOfHouses 550
#define xOfLeftHouse 50
#define xOfRightHouse 750
#define stepBetweenHouses (xOfRightHouse - xOfLeftHouse) / housesN

#define widthOfTriangle  40
#define heightOfTriangle 200


#define betweenHeightOfWhiteAndBlack 20

#define windowHeight 600
#define windowWidth 760

#define checkerR 18
#define checkersNum 15

#define maxDiceMean 6

#define diceSize 40
#define diceDotSize 5
#define diceDotOffset 6

#define pauseRoll 2000    //TOUSER pause in rolling. safe configure
#define pauseStep 1   //TOUSER pause in enemy's step (fps while checker moving)
#endif // !NARD_DEFS_HPP

