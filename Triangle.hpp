#ifndef NARD_TRIANGLE_TYPES_HPP
#define NARD_TRIANGLE_TYPES_HPP

#include <array>
#include "Printable.hpp"
#include "Point.hpp"

//TOUSER just Triangle
class Triangle :
  public Printable
{
public:
  Triangle(std::array <Point, 3>&& vertices);
  void print() const override;

  size_t getCenterX() const;
  size_t getLowestY() const;
protected:
  std::array <Point, 3> vertices_;
};
#endif // !NARD_TRIANGLE_TYPES_HPP
