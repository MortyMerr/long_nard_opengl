#include "Enemy.hpp"
#include <glut.h>
#include <iostream>
#include <cmath>
#include <conio.h>
#include "GraphicUtils.hpp"
#include "Game.hpp"
#include "defs.hpp"
#include "Map.hpp"

int Enemy::step(size_t oldX, size_t oldY, size_t x, size_t y, Dice& dice, std::unique_ptr <Checker>& checker) const
{
  //TOUSER u should not wathc here and trying to understand
  //it's very badcoded II. U should only know, that II for enemy
  //realized here
  bool first = false;
  switch (level_) {
  case(Level::low):
  {
    if (pos_ == House::position::up) {
      int index = housesN - 1, subindex = 0;
      while (true) {
        bool fromUp = false;
        bool found = false;
        if (index == -1) {
          goto badcode;
        }
        for (; index > -1; index--) {
          try {
            checker = std::move(
              Map::instance().getCheckerFromHouse({ House::position::up, index }, color_));
            first = (index == housesN - 1);
            found = true;
            fromUp = true;
          }
          catch (const std::out_of_range& e) {
            std::cerr << e.what();
            if (index != 0) {
              continue;
            }
          }

          if (!found) {
          badcode: for (; subindex < housesN; subindex++) {
            try {
              checker = std::move(
                Map::instance().getCheckerFromHouse({ House::position::down, subindex }, color_));
              if (subindex == housesN - 1) {
                dice.dices_.pop_back();
                movingChecker_ = std::move(checker);
                targetIndex_ = -1;
                targetPosition_ = House::position::up;
                target_ = { movingChecker_->center_.x + 30, movingChecker_->center_.y };
                return 0;
              }
              found = true;
              fromUp = false;
              break;
            }
            catch (const std::out_of_range& e) {
              std::cerr << e.what();
              continue;
            }
          }
          }

          if (!found) {
            return -1;
          }

          if (!(fromFirst_ && first)) {
            for (int i = index - 1; i > -1; i--) {
              const House& target = Map::instance().getHouse(
              { House::position::up, i });
              if ((target.getCheckersColor() == checker->getColor()
                || target.getCheckersColor() == Checker::Color::no)
                && dice.step(index - i)) {
                movingChecker_ = std::move(checker);
                targetIndex_ = i;
                targetPosition_ = target.getPosition();
                target_ = target.getFreePoint();
                fromFirst_ = first;
                return 0;
              }
            }
          }
          else {
            goto returnChecker;
          }

          if (fromUp) {
            for (int i = 0; i < housesN - 1; i++) {
              const House& target = Map::instance().getHouse(
              { House::position::down, i });
              if ((target.getCheckersColor() == checker->getColor()
                || target.getCheckersColor() == Checker::Color::no)
                && dice.step(index + i + 1)) {
                movingChecker_ = std::move(checker);
                targetIndex_ = i;
                targetPosition_ = target.getPosition();
                target_ = target.getFreePoint();
                return 0;
              }
            }
          }
          else
          {
            for (int i = subindex + 1; i < housesN; i++) {
              const House& target = Map::instance().getHouse(
              { House::position::down, i });
              if ((target.getCheckersColor() == checker->getColor()
                || target.getCheckersColor() == Checker::Color::no)
                && dice.step(i - subindex)) {
                movingChecker_ = std::move(checker);
                targetIndex_ = i;
                targetPosition_ = target.getPosition();
                target_ = target.getFreePoint();
                subindex = 0;
                return 0;
              }
            }
            if (subindex == housesN - 1) {
              //Not found
              return 4;
            }
            const auto from = Map::instance().getHouse(checker->center_.x, checker->center_.y);
            Map::instance().pushCheckerToHouse(checker, from);
            subindex++;
            goto badcode;
          }
        returnChecker: const auto from = Map::instance().getHouse(checker->center_.x, checker->center_.y);
          Map::instance().pushCheckerToHouse(checker, from);
        }
      }
    }
    else/////////////////////////////////////////////////
    {
      int index = 0, subindex = housesN - 1;
      while (true) {
        bool fromUp = false;
        bool found = false;
        if (index == -1) {
          goto badcode2;
        }
        for (; index < housesN; index++) {
          try {
            checker = std::move(
              Map::instance().getCheckerFromHouse({ House::position::down, index }, color_));
            first = (index == 0);
            found = true;
            fromUp = true;
          }
          catch (const std::out_of_range& e) {
            std::cerr << e.what();
            if (index != housesN - 1) {
              continue;
            }
            found = false;
          }

          if (!found) {
          badcode2: for (; subindex > -1; subindex--) {
            try {
              checker = std::move(
                Map::instance().getCheckerFromHouse({ House::position::up, subindex }, color_));
              if (subindex == 0) {
                dice.dices_.pop_back();
                movingChecker_ = std::move(checker);
                targetIndex_ = -1;
                targetPosition_ = House::position::up;
                target_ = { movingChecker_->center_.x + 30, movingChecker_->center_.y };
                return 0;
              }
              found = true;
              fromUp = false;
              break;
            }
            catch (const std::out_of_range& e) {
              std::cerr << e.what();
              continue;
            }
          }
          }

          if (!found) {
            return -1;
          }

          if (!(fromFirst_ && first)) {
            for (int i = index + 1; i < housesN - 1; i++) {
              const House& target = Map::instance().getHouse(
              { House::position::down, i });
              if ((target.getCheckersColor() == checker->getColor()
                || target.getCheckersColor() == Checker::Color::no)
                && dice.step(i - index)) {
                movingChecker_ = std::move(checker);
                targetIndex_ = i;
                targetPosition_ = target.getPosition();
                target_ = target.getFreePoint();
                fromFirst_ = first;
                return 0;
              }
            }
          }
          else {
            goto returnChecker2;
          }

          if (fromUp) {
            for (int i = housesN - 1; i > 0; i--) {
              const House& target = Map::instance().getHouse(
              { House::position::up, i });
              if ((target.getCheckersColor() == checker->getColor()
                || target.getCheckersColor() == Checker::Color::no)
                && dice.step(2 * housesN - index - 1 - i)) {
                movingChecker_ = std::move(checker);
                targetIndex_ = i;
                targetPosition_ = target.getPosition();
                target_ = target.getFreePoint();
                return 0;
              }
            }
          }
          else
          {
            for (int i = subindex - 1; i > -1; i--) {
              const House& target = Map::instance().getHouse(
              { House::position::up, i });
              if ((target.getCheckersColor() == checker->getColor()
                || target.getCheckersColor() == Checker::Color::no)
                && dice.step(subindex - i)) {
                movingChecker_ = std::move(checker);
                targetIndex_ = i;
                targetPosition_ = target.getPosition();
                target_ = target.getFreePoint();
                subindex = 0;
                return 0;
              }
            }
            if (index == housesN - 1) {//fixed
              //Not found
              const auto from = Map::instance().getHouse(checker->center_.x, checker->center_.y);
              Map::instance().pushCheckerToHouse(checker, from);
              return 4;
            }
            const auto from = Map::instance().getHouse(checker->center_.x, checker->center_.y);
            Map::instance().pushCheckerToHouse(checker, from);
            subindex++;
            goto badcode2;
          }
        returnChecker2: const auto from = Map::instance().getHouse(checker->center_.x, checker->center_.y);
          Map::instance().pushCheckerToHouse(checker, from);
        }
      }
    }
    break;
  }
  case(Level::high):
    break;
    return 0;
  }
}

std::string Enemy::whoseTurn() const
{
  return "Enemy turn";
}

void Enemy::setLevel(Level&& level)
{
  level_ = std::move(level);
}

bool Enemy::movingChecker()
{
  if (movingChecker_.get() == nullptr) {
    return true;
  }

  if (movingChecker_->center_.x == target_.x
    && movingChecker_->center_.y == target_.y) {
    Map::instance().pushCheckerToHouse(movingChecker_, { targetPosition_, targetIndex_ });
    return true;
  }
  const auto x = static_cast<int>(target_.x) - static_cast<int>(movingChecker_->center_.x);
  const auto dx = (x > 0) - (x < 0);
  const auto y = static_cast<int>(target_.y) - static_cast<int>(movingChecker_->center_.y);
  const auto dy = (y > 0) - (y < 0);
  movingChecker_->move(static_cast<int>(movingChecker_->center_.x) + dx,
    static_cast<int>(movingChecker_->center_.y) + dy);
  movingChecker_->print();
  return false;
}


Enemy::Enemy(Level level, House::position pos) :
  level_(level),
  pos_(pos)
{}