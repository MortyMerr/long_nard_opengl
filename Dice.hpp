#ifndef NARD_DICE_HPP
#define NARD_DICE_HPP

#include <vector>
#include "Printable.hpp"


class Dice : public Printable
{
public:
  Dice(size_t count);

  std::vector <unsigned int> dices_;
  void print() const override;

  //TOUSER ���� �������� ������� �� ��������� ������� ���������� �������
  //������� ������ ����������� ��� ������ � ���������� true
  bool step(int difference);
private:
  void printDice(int x, int y, size_t mean) const;
};

#endif // !NARD_DICE_HPP