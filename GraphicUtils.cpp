#include "GraphicUtils.hpp"
#include <iostream>
#include "Map.hpp"
#include "defs.hpp"

bool moving = false;
std::unique_ptr <Checker> movingChecker(nullptr);
int pressX = 0, pressY = 0;

void initialize()//������� - �� ��������(�� ���� ��� ����������� ������� ����������, ��� � �������� �������)
{
  glClearColor(0.5, 0.5, 0.5, 0.5);//���� �������		
  glMatrixMode(GL_PROJECTION);
  //glOrtho(0.0, windowWidth, 0.0, windowHeight, 0, 0);	//��������� �������	
}

void drawText(std::string&& text, int x, int y) 
{
  glMatrixMode(GL_PROJECTION);
  double *matrix = new double[16];
  glGetDoublev(GL_PROJECTION_MATRIX, matrix);
  glLoadIdentity();
  gluOrtho2D(0, windowWidth - 20, 0, windowHeight);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glPushMatrix();
  glLoadIdentity();
  glRasterPos2i(x, y);
  for (size_t i = 0; i < text.size(); i++)
  {
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)text[i]);
  }
  glPopMatrix();
}

void reshape(int w, int h)
{
  //glViewport(0, 0, w, h);
  //glMatrixMode(GL_PROJECTION);
  //glLoadIdentity();
  gluOrtho2D(0, w, 0, h);
  //glutReshapeWindow(windowWidth - 20, windowHeight);
  //glMatrixMode(GL_MODELVIEW);
  //glLoadIdentity();
}

void draw() {//��� ������� ���������� ������ ���, ������������� ���������� ������
  glClear(GL_COLOR_BUFFER_BIT);//�������� �����
  Game::instance().print();
  if (moving && movingChecker.get() != nullptr) {
    movingChecker->print();
  }
  glutSwapBuffers();//������� ������������ �� �����
  glutPostRedisplay();
}

void mouseButton(int button, int state, int x, int y)
{
  y = windowHeight - y; //TOREMEMBER ��� �� �������, ��� ����� ������� ������� ��������� �������
  if (button == GLUT_MIDDLE_BUTTON
    &&  state == GLUT_DOWN) {
    Game::instance().changeLevel();
  }
  if (button == GLUT_RIGHT_BUTTON
    && state == GLUT_DOWN) {
    Game::instance().start();
  }

  if (button == GLUT_LEFT_BUTTON) {
    if (y < 40 && state == GLUT_DOWN) {
      Game::instance().forceStepDone();
      return;
    }
    if ((Game::instance().getStage() == Game::Stage::first_step
      || Game::instance().getStage() == Game::Stage::second_step)) {
      if (state == GLUT_DOWN) {
        pressX = x;
        pressY = y;
        try {
          movingChecker = std::move(Map::instance().getCheckerFromHouse(
            Map::instance().getHouse(x, y), Game::instance().getActive()->getColor()));
        }
        catch (const std::out_of_range e) {
          std::cerr << e.what();
          return;
        }
        moving = true;
      }

      if (state == GLUT_UP && moving) {
        moving = false;
        if (Game::instance().getActive()->step(pressX, pressY, x, y, Game::instance().getDice(), movingChecker)
          == 0) {
          Game::instance().stepDone();
        }
        else {
          const auto from = Map::instance().getHouse(pressX, pressY);
          Map::instance().pushCheckerToHouse(movingChecker, from);
        }
      }
    }
  }
}

void mouseMove(int x, int y)
{
  if (moving)
  {
    y = windowHeight - y; //TOREMEMBER ��� �� �������, ��� ����� ������� ������� ��������� �������
    movingChecker->move(x, y);
  }
}
