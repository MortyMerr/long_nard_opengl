#ifndef NARD_YOU_HPP
#define NARD_YOU_HPP

#include "Player.hpp"

//real player (your) class
class You : public Player
{
public:
  int step(size_t oldX, size_t oldY, size_t x, size_t y, Dice& dice, std::unique_ptr <Checker>& checker) const override;
  std::string whoseTurn() const override;
  //TODO shouldn't be here
  bool movingChecker() override;
};

#endif // !NARD_YOU_HPP
