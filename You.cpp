#include "You.hpp"
#include <glut.h>
#include "Map.hpp"
#include "defs.hpp"

int You::step(size_t oldX, size_t oldY, size_t x, size_t y, Dice& dice, std::unique_ptr <Checker>& checker) const
{
  const auto from = Map::instance().getHouse(oldX, oldY);
  const auto target = Map::instance().getHouse(x, y);

  if (checker->getColor() != Map::instance().isHouseEmpty(target)
    && Map::instance().isHouseEmpty(target) != Checker::Color::no) {
    return 1; //������� �������� � �� ����� ������
  }

  //�������� �� ��, ����� �� �������� ���������� ��������
  int difference = 0;
  if (from.first == target.first) {
    difference = abs(static_cast <int>(from.second) - static_cast <int>(target.second));
  }
  else {
    difference = abs(housesN - static_cast <int>(from.second))
      + abs(housesN - static_cast <int>(target.second)) - 1;
  }

  //check first/last house
  if (from.first == House::position::down
    && from.second == 0 && fromFirst_) {
      return 1;
  }
  else if (from.first == House::position::up
    && from.second == 0) {
    dice.dices_.pop_back();
    Map::instance().pushCheckerToHouse(checker, { House::position::up, -1 });
    return 0;
  }

  if (!dice.step(difference)) {
    return 1; //���������� �������� �������� � ��������� ������ ����
  }

  if (from.first == House::position::down
    && from.second == 0 && !fromFirst_) {
    fromFirst_ = true;
  }

  Map::instance().pushCheckerToHouse(checker, target);
  return 0; //��� ������� ������
}

std::string You::whoseTurn() const
{
  return "Your turn";
}

bool You::movingChecker()
{
  //TODO don't know what
  return true;
}
