#ifndef  PLAYER_HPP
#define PLAYER_HPP

#include <memory>
#include "Dice.hpp"
#include "Checker.hpp"

//TOUSER parent for all players
class Player
{
public:
  Player();
  mutable bool fromFirst_;

  virtual ~Player() = default;
  virtual int step(size_t oldX, size_t oldY, size_t x, size_t y, Dice& dice, std::unique_ptr <Checker>& checker) const = 0;
  virtual std::string whoseTurn() const = 0;
  virtual bool movingChecker() = 0;

  void setColor(Checker::Color&& color);
  const Checker::Color& getColor() const;
protected:
  int checkerCount_;
  Checker::Color color_;
};


#endif // ! PLAYER_HPP