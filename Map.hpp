#ifndef NARD_MAP_HPP
#define NARD_MAP_HPP

#include <array>
#include "Printable.hpp"
#include "House.hpp"

//TOUSER map class, checkers && background here
class Map :
  public Printable
{
public:
  //TOUSER pattern singleton(google it, if interested)
  Map(const Map &) = delete;
  Map(Map &&) = delete;
  Map& operator =(const Map &) = delete;
  Map& operator =(Map &&) = delete;
  static Map& instance();
  
  void print() const override;
  
  //TOUSER ���������� ���, �������� ������������� ����������
  Checker::Color isHouseEmpty(const std::pair <House::position, size_t>& house) const;
  
  std::pair <House::position, size_t> getHouse(size_t x, size_t y) const;
  const House& getHouse(std::pair <House::position, size_t>&& house) const;
  
  std::unique_ptr <Checker> getCheckerFromHouse(std::pair <House::position, size_t>&& house, const Checker::Color& color);
  void pushCheckerToHouse(std::unique_ptr <Checker>& checker, const std::pair <House::position, size_t>& house);
private:
  Map();
  std::vector <House> housesUp_, housesDown_; //TOUSER ���� ������������� - ������ � ������� ���� �������������
                                               //�� ���� ��, �� �� ������ ���������� ��� ����������, ��� ��� ����� ������
  void printHouseLayer(const std::vector <House>& houses, bool flagUpOrDown) const;
  void printFrame() const;
};

#endif // NARD_PRINTABLE