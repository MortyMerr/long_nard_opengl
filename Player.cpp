#include "Player.hpp"
#include "defs.hpp"

Player::Player() :
  color_(Checker::Color::no),
  checkerCount_(checkersNum),
  fromFirst_(false)
{}

void Player::setColor(Checker::Color&& color)
{
  color_ = color;
}

const Checker::Color& Player::getColor() const
{
  return color_;
}