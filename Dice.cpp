#include "Dice.hpp"
#include <glut.h>
#include "defs.hpp"

Dice::Dice(size_t count)
{
  for (size_t i = 0; i < count; i++) {
    dices_.push_back(static_cast <unsigned int>(rand() % maxDiceMean + 1));
  }
  if (count == 2) {
    //TOUSER TO GENERATE doubles uncondittionally use next statement
    //dices_[0] = dices_[1];
    if (dices_[0] == dices_[1]) {
      dices_.push_back(dices_[0]);
      dices_.push_back(dices_[0]);
    }
  }
}

void Dice::print() const
{
  for (size_t i = 0; i < dices_.size(); i++) {
    printDice(windowWidth / 2 - diceSize + i * (diceSize + 6), windowHeight / 2, dices_[i]);
  }
}

bool Dice::step(int difference)
{
  //TODO ��������� ������
  if (difference == dices_[0]) {
    dices_.erase(dices_.begin());
    return true;
  }
  else if (dices_.size() == 2 && difference == dices_[1]) {
    dices_.pop_back();
    return true;
  }
  else if (dices_.size() == 2 && difference == (dices_[0] + dices_[1])) {
    dices_.clear();
    return true;
  }
  return false;
}

void Dice::printDice(int x, int y, size_t mean) const
{
  //glColor3f(0.1, 0.1, 0.1);

  glLineWidth(2);
  glBegin(GL_LINE_STRIP);
  glVertex2f(x - diceSize / 2, y - diceSize / 2);
  glVertex2f(x - diceSize / 2, y + diceSize / 2);
  glVertex2f(x + diceSize / 2, y + diceSize / 2);
  glVertex2f(x + diceSize / 2, y - diceSize / 2);
  glVertex2f(x - diceSize / 2, y - diceSize / 2);
  glEnd();

  glPointSize(diceDotSize);
  glBegin(GL_POINTS);

  //break ���������� ��������, ��� ��������� ������� �� ��������� �����
  switch (mean) {
  case 6:
    glVertex2f(x + diceSize / 2 - diceDotOffset, y - diceSize / 2 + diceDotOffset);
  case 5:
    glVertex2f(x - diceSize / 2 + diceDotOffset, y - diceSize / 2 + diceDotOffset);
  case 4:
    glVertex2f(x + diceSize / 2 - diceDotOffset, y - diceSize / 2 + diceSize / 3 + diceDotOffset);
  case 3:
    glVertex2f(x - diceSize / 2 + diceDotOffset, y - diceSize / 2 + diceSize / 3 + diceDotOffset);
  case 2:
    glVertex2f(x + diceSize / 2 - diceDotOffset, y - diceSize / 2 + diceSize / 3 * 2 + diceDotOffset);
  case 1:
    glVertex2f(x - diceSize / 2 + diceDotOffset, y - diceSize / 2 + diceSize / 3 * 2 + diceDotOffset);
  }
  glEnd();
}
