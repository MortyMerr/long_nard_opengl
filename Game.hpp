#ifndef NARD_GAME_HPP
#define NARD_GAME_HPP

#include <memory>
#include "Player.hpp"
#include "Printable.hpp"
#include "Dice.hpp"

//TOUSER Big class with all game logic

class Game : public Printable
{
public:
  //TOUSER pattern singleton(google it, if interested)
  enum class gameMode : int {
    playerVsLow = 0, playerVsHigh = 1, lowVsLow = 2, highVsLow = 3};
  enum class Stage : int { mode_selecting,
    not_started, rolling_first_dice, rolling_second_dice, reroll,
    roll_results, first_step, second_step, analyzing,  winners
  };
  enum class stepStage { init, waiting, done };

  Game(const Game &) = delete;
  Game(Game &&) = delete;

  Game& operator =(const Game &) = delete;
  Game& operator =(Game &&) = delete;
  static Game& instance();

  const Stage& getStage() const;
  void print() const override;
  std::unique_ptr <Player>& getActive() const;
  Dice& getDice() const;
  void start();
  void stepDone();
  void forceStepDone();
  void changeLevel();
private:
  Game();
  mutable size_t pause_;
  mutable Stage stage_;
  mutable stepStage stepStage_;
  mutable gameMode gameMode_;
  mutable Dice dice_;
  mutable std::unique_ptr <Player> you_, him_;
};

#endif // !NARD_GAME_HPP