#include "Triangle.hpp"
#include <algorithm>
#include <glut.h>

Triangle::Triangle(std::array <Point, 3>&& vertices) : 
  vertices_(std::move(vertices))
{
}

void Triangle::print() const
{
  glBegin(GL_TRIANGLES);  // ���������, ��� ����� ��������
  std::for_each(std::cbegin(vertices_), std::cend(vertices_), [](const Point& point)
  {
    glVertex2d(point.x, point.y);
  });
  /*for (size_t i = 0; i < 3; i++) {
    glVertex2d(vertices_[i].x, vertices_[i].y);
  }*/
  glEnd();               // ��������� �������� ��������
}

size_t Triangle::getCenterX() const
{
  return vertices_[2].x;
}

size_t  Triangle::getLowestY() const
{
  return vertices_[0].y;
}