#include "Game.hpp"
#include <glut.h>
#include <Windows.h>
#include <time.h>
#include "Map.hpp"
#include "Enemy.hpp"
#include "GraphicUtils.hpp"
#include "You.hpp"
#include "defs.hpp"

Game::Game() :
  pause_(pauseRoll),
  stage_(Stage::not_started),
  you_(std::make_unique <You>()),
  him_(std::unique_ptr <Player>(new Enemy(Enemy::Level::low, House::position::up))),
  dice_({ 0 })
{
  //TODO random this
  you_->setColor(Checker::Color::white);
  him_->setColor(Checker::Color::black);
}

Game& Game::instance()
{
  static Game s;
  return s;
}

void Game::start()
{
  if (stage_ == Stage::not_started) {
    stage_ = Stage::mode_selecting;
  }
  else
    if (stage_ == Stage::mode_selecting) {
      stage_ = Stage::rolling_first_dice;
      switch (gameMode_) {
      case gameMode::highVsLow:
        you_ = std::unique_ptr <Player>(new Enemy(Enemy::Level::low, House::position::down));//fixme
        break;
      case gameMode::lowVsLow:
        you_ = std::unique_ptr <Player>(new Enemy(Enemy::Level::low, House::position::down));
        break;
      case gameMode::playerVsHigh:
        him_ = std::unique_ptr <Player>(new Enemy(Enemy::Level::high, House::position::up));
        break;
      case gameMode::playerVsLow:
        break;
      }
      you_->setColor(Checker::Color::white);
      him_->setColor(Checker::Color::black);
      pause_ = pauseRoll;
    }
}


void Game::print() const
{
  Sleep(pause_);
  static Dice dice1(1), dice2(1);
  Map::instance().print();
  switch (stage_) {
  case(Stage::not_started):
    glColor3f(0, 0, 0);
    drawText("Please, press right_mouse_button to start", 200, 300);
    glColor3f(1, 1, 1);
    break;

  case(Stage::mode_selecting):
    pause_ = 0;
    switch (gameMode_) {
    case gameMode::playerVsLow:
      drawText("Player vs low II", 250, 300);
      break;
    case gameMode::playerVsHigh:
      drawText("Player vs high II", 250, 300);
      break;
    case gameMode::lowVsLow:
      drawText("low II vs low II", 250, 300);
      break;
    case gameMode::highVsLow:
      drawText("low II vs high II", 250, 300);
      break;
    }
    break;

  case(Stage::rolling_first_dice):
    glColor3f(0, 0, 0);
    drawText("Rolling, whose turn first, enemy's points", 200, 250);
    glColor3f(0, 0, 0);
    dice1.print();
    stage_ = Stage::rolling_second_dice;
    break;

  case(Stage::rolling_second_dice):
    glColor3f(1, 1, 1);
    drawText("Rolling, whose turn first, your points", 200, 250);
    glColor3f(1, 1, 1);
    dice2.print();
    stage_ = Stage::analyzing;
    break;

  case(Stage::analyzing):
    if (dice1.dices_[0] == dice2.dices_[0]) {
      stage_ = Stage::reroll;
    }
    else
    {
      stage_ = Stage::roll_results;
    }
    break;

  case(Stage::reroll):
    glColor3f(0, 0, 0);
    drawText("Reroll", 350, 300);
    dice1 = { 1 };
    dice2 = { 1 };
    stage_ = Stage::rolling_first_dice;
    break;

  case(Stage::roll_results):
    glColor3f(0, 0, 0);
    if (dice1.dices_[0] > dice2.dices_[0]) {
      drawText("You lose rolling", 320, 280);
      stage_ = Stage::second_step;
    }
    else {
      drawText("You won rolling", 320, 280);
      stage_ = Stage::first_step;
    }
    break;

  case(Stage::first_step):
    pause_ = 0;
    if (gameMode_ == gameMode::highVsLow
      || gameMode_ == gameMode::lowVsLow) {
      std::swap(you_, him_);
      stage_ = Stage::second_step;
      break;
    }
    switch (stepStage_) {
    case(stepStage::init):
      dice_ = Dice(2);
      you_->fromFirst_ = false;
      stepStage_ = stepStage::waiting;
      break;
    case(stepStage::waiting):
      break;
    case(stepStage::done):
      stepStage_ = stepStage::init;
      stage_ = Stage::second_step;
      break;
    }
    glColor3f(1, 1, 1);
    drawText(you_->whoseTurn(), 200, 300);
    dice_.print();
    break;

  case(Stage::second_step):
    pause_ = pauseStep;
    switch (stepStage_) {
    case(stepStage::init):
    {
      him_->fromFirst_ = false;
      dice1 = dice_ = Dice(2);
      const auto stepResult = him_->step(0, 0, 0, 0, dice_, std::unique_ptr <Checker>(nullptr));
      stepStage_ = stepStage::waiting;
      if (stepResult == -1) {
        stage_ = Stage::winners;
      }
      else if (stepResult == 4) {
        stepStage_ = stepStage::done;
      }
      break;
    }

    case(stepStage::waiting):
      //FIXME
      if (him_->movingChecker()) {
        if (dice_.dices_.empty()) {
          stepStage_ = stepStage::done;
        }
        else {
          const auto stepResult = him_->step(0, 0, 0, 0, dice_, std::unique_ptr <Checker>(nullptr));
          if (stepResult == -1) {
            stage_ = Stage::winners;
          }
          else if (stepResult == 4) {
            stepStage_ = stepStage::done;
          }
        }
      }
      break;
    case(stepStage::done):
      stepStage_ = stepStage::init;
      stage_ = Stage::first_step;
      break;
    }
    glColor3f(0, 0, 0);
    drawText(him_->whoseTurn(), 200, 300);
    dice1.print();
    break;

  case(Stage::winners):
    glColor3f(0, 0, 0);
    drawText("End", 300, 300);
    pause_ = pauseRoll;
    break;
  }
}


const Game::Stage& Game::getStage() const
{
  return stage_;
}

std::unique_ptr <Player>& Game::getActive() const
{
  return you_;
}

void Game::stepDone()
{
  if (dice_.dices_.empty()) {
    stepStage_ = stepStage::done;
  }
}

void Game::forceStepDone()
{
  dice_.dices_.clear();
  stepDone();
}


Dice& Game::getDice() const
{
  return dice_;
}

void Game::changeLevel()
{
  auto mode = static_cast<int>(gameMode_) + 1;
  if (mode == 4) {
    mode = 0;
  }
  gameMode_ = static_cast<gameMode>(mode);
}