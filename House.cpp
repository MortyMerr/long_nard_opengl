#include "House.hpp"
#include <stdexcept>
#include "defs.hpp"

House::House(Triangle&& triangle, position pos) :
  triangle_(std::move(triangle)),
  pos_(pos)
{}

void House::print() const
{
  triangle_.print();
}

void House::printCheckers() const
{
  std::for_each(std::cbegin(checkers_), std::cend(checkers_), [](const auto& checker)
  {
    checker->print();
  });
}

void House::push(Checker&& checker)
{
  checkers_.push_back(std::make_unique <Checker> (std::move(checker)));
}

std::unique_ptr <Checker> House::pop()
{
  if (checkers_.size() == 0) {
    throw std::out_of_range("click_on_empty_house\n");
  }
  auto tmp = std::move(checkers_.back());
  checkers_.pop_back();
  return tmp;
}

Point House::getFreePoint() const
{
  return Point{ triangle_.getCenterX(), triangle_.getLowestY()
    + static_cast<int>(pos_) * (checkers_.size() * (checkerR * 2 - (checkerR + 3)) + checkerR + 5) };
}

double House::getEuclidianMetric(size_t x, size_t y) const
{
  return sqrt(pow(static_cast<double>(x) - static_cast<double>(triangle_.getCenterX()), 2)
    + pow(static_cast<double>(y) - static_cast<double>(triangle_.getLowestY()), 2));
}

Checker::Color House::getCheckersColor() const
{
  if (checkers_.size() == 0) {
    return Checker::Color::no;
  }
  return checkers_[0]->getColor();
}

void House::push(std::unique_ptr <Checker>& checker)
{
  checker->move(getFreePoint());
  checkers_.push_back(std::move(checker));
}

const House::position& House::getPosition() const
{
  return pos_;
}
