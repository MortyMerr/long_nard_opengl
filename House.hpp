#ifndef NARD_HOUSE_HPP
#define NARD_HOUSE_HPP

#include "Printable.hpp"
#include "Triangle.hpp"
#include "Checker.hpp"
#include <vector>
#include <memory>

class House : //TOUSER �� ����� ���� ��� �� ���, ��� ���� �� ������������� �� ������� ����� � ���
              //� ������ �� ���� ��� �������
  public Printable
{
public:
  enum class position : int {up = -1, down = 1};
  House(Triangle&& triangle, position pos);
  void print() const override;
  void House::printCheckers() const;

  void push(Checker&& checker);
  void push(std::unique_ptr <Checker>& checker);

  std::unique_ptr <Checker>  pop();

  Point getFreePoint() const;
  Checker::Color getCheckersColor() const;
  const House::position& getPosition() const;
  //TOUSER ���������� ���������� �� ���������� ��������� �� ����
  double getEuclidianMetric(size_t x, size_t y) const;
private:
  Triangle triangle_;
  std::vector < std::unique_ptr<Checker> > checkers_;
  position pos_;
  //TODO color of chechers in house getter
};
#endif // !NARD_HOUSE_HPP
