#ifndef NARD_GRAPHIC_UTILS_HPP
#define NARD_GRAPHIC_UTILS_HPP
#include <string>
#include <glut.h>
#include <memory>
#include "Game.hpp"
//TOUSER in fact, you should't try to understand this. Opengl - sucks

extern bool moving;
extern std::unique_ptr <Checker> movingChecker;
extern int pressX, pressY;

void initialize();
void drawText(std::string&& text , int x, int y);
void reshape(int w, int h);
void draw();
void mouseButton(int button, int state, int x, int y);
void mouseMove(int x, int y);

#endif // !NARD_GRAPHIC_UTILS_HPP
