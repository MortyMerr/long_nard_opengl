#include "Map.hpp"
#include <glut.h>
#include "GraphicUtils.hpp"
#include "defs.hpp"

Map::Map()
{
  for (size_t i = 0; i < housesN; i++) {
    auto tmp = i & 1;
    //last dot in Triangle must always have center x coord
    housesDown_.push_back(House(Triangle(
    { {
    {xOfLeftHouse + stepBetweenHouses * i - widthOfTriangle / 2, yOfLowLayerOfHouses },
    {xOfLeftHouse + stepBetweenHouses * i + widthOfTriangle / 2, yOfLowLayerOfHouses},
    {xOfLeftHouse + stepBetweenHouses * i, yOfLowLayerOfHouses + heightOfTriangle - (i & 1) * betweenHeightOfWhiteAndBlack }
    } }), House::position::down));
    housesUp_.push_back(House(Triangle(
    { {
      { xOfLeftHouse + stepBetweenHouses * i - widthOfTriangle / 2, yOfUpLayerOfHouses },
      { xOfLeftHouse + stepBetweenHouses * i + widthOfTriangle / 2, yOfUpLayerOfHouses },
      { xOfLeftHouse + stepBetweenHouses * i, yOfUpLayerOfHouses - heightOfTriangle + ((i + 1) & 1) * betweenHeightOfWhiteAndBlack }
      } }), House::position::up));
  }
  for (size_t i = 0; i < checkersNum; i++) {
    housesDown_[0].push(Checker(Checker::Color::white, housesDown_[0].getFreePoint()));
    housesUp_[housesN - 1].push(Checker(Checker::Color::black, housesUp_[housesN - 1].getFreePoint()));
  }
}

void Map::printHouseLayer(const std::vector <House>& houses, bool flagUp) const
{
  size_t i = flagUp;
  std::for_each(std::cbegin(houses), std::cend(houses), [&i](const House& house) mutable
  {
    if (i & 1) {
      glColor3f(0.2, 0.2, 0.2);
    }
    else {
      glColor3f(0.9, 0.9, 0.9);
    }
    i++;
    house.print();
  });
}

void Map::print() const
{
  printHouseLayer(housesUp_, true);
  printHouseLayer(housesDown_, false);
  printFrame();

  for (size_t i = 0; i < housesN; i++) {
    housesDown_[i].printCheckers();
    housesUp_[i].printCheckers();
  }
}

Map& Map::instance()
{
  static Map s;
  return s;
}

void Map::printFrame() const
{
  glLineWidth(10);
  glBegin(GL_LINE_STRIP);

  glColor3f(0.15, 0.15, 0.15);
  glVertex2d(10, 40);
  glVertex2d(10, windowHeight - 40);
  glVertex2d(windowWidth - 31, windowHeight - 40);
  glVertex2d(windowWidth - 31, 40);
  glVertex2d(10, 40);
  glEnd();

  //��������� �����
  glLineWidth(8);
  glBegin(GL_LINE_STRIP);
  glColor3f(0.1, 0.1, 0.1);
  glVertex2d(xOfLeftHouse - widthOfTriangle / 2 - 10, yOfLowLayerOfHouses);
  glVertex2d(xOfLeftHouse - widthOfTriangle / 2 - 10, yOfUpLayerOfHouses);
  glVertex2d(xOfRightHouse - 31, yOfUpLayerOfHouses);
  glVertex2d(xOfRightHouse - 31, yOfLowLayerOfHouses);
  glVertex2d(xOfLeftHouse - widthOfTriangle / 2 - 10, yOfLowLayerOfHouses);
  // glVertex2d(windowWidth - 10, windowHeight - 10);
   //glVertex2d(windowWidth - 10, 10);
   //glVertex2d(10, 10);
  glEnd();
  glColor3f(0, 0, 0);
  //drawText("Click here, if you want end your step", 200, 10);
}

std::pair <House::position, size_t> Map::getHouse(size_t x, size_t y) const
{
  size_t nearestHouse = 0;
  House::position position = House::position::down;
  double metric = std::numeric_limits <double>::max();
  for (size_t i = 0; i < housesN; i++)
  {
    if (housesDown_[i].getEuclidianMetric(x, y) < metric) {
      nearestHouse = i;
      position = House::position::down;
      metric = housesDown_[i].getEuclidianMetric(x, y);
    }
    if (housesUp_[i].getEuclidianMetric(x, y) < metric) {
      nearestHouse = i;
      position = House::position::up;
      metric = housesUp_[i].getEuclidianMetric(x, y);
    }
  }
  return{ position, nearestHouse };
}

Checker::Color Map::isHouseEmpty(const std::pair <House::position, size_t>& house) const
{
  switch (house.first) {
  case House::position::down:
    return housesDown_[house.second].getCheckersColor();
    break;
  case House::position::up:
    return housesUp_[house.second].getCheckersColor();
    break;
  }
}

std::unique_ptr <Checker> Map::getCheckerFromHouse(std::pair <House::position, size_t>&& house,
  const Checker::Color& color)
{
  if (house.first == House::position::up) {
    if (housesUp_[house.second].getCheckersColor() == color) {
      return std::move(housesUp_[house.second].pop());
    }
  }
  else if (housesDown_[house.second].getCheckersColor() == color) {
    return std::move(housesDown_[house.second].pop());
  }
  throw std::out_of_range("click_on_enemies_house\n");
}

void Map::pushCheckerToHouse(std::unique_ptr <Checker>& checker, const std::pair <House::position, size_t>& house)
{
  if (house.second == -1) {
    checker.release();
    return;
  }

  if (house.first == House::position::up) {
    housesUp_[house.second].push(checker);
  }
  else
  {
    housesDown_[house.second].push(checker);
  }
}

const House& Map::getHouse(std::pair <House::position, size_t>&& house) const
{
  if(house.first == House::position::up){
    return housesUp_[house.second];
  }
  return housesDown_[house.second];
}
