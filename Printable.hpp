#ifndef NARD_PRINTABLE_HPP
#define NARD_PRINTABLE_HPP

//TOUSER parent for all printable objects
class Printable
{
public:
  virtual ~Printable() = default;
  virtual  void print() const = 0;
};

#endif // !NARD_PRINTABLE_HPP