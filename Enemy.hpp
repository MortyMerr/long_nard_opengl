#ifndef NARD_ENEMY_HPP
#define NARD_ENEMY_HPP

#include "Player.hpp"
#include "House.hpp"

//TOUSER Enemy. bad class && code, don't look
class Enemy :
  public Player
{
public:
  enum class Level {low, high};
  Enemy(Level level, House::position pos);
  int step(size_t oldX, size_t oldY, size_t x, size_t y, Dice& dice, std::unique_ptr <Checker>& checker) const override;
  std::string whoseTurn() const override;

  void setLevel(Level&& level);
  bool movingChecker();
private:
  Level level_;
  House::position pos_;
  mutable std::unique_ptr <Checker> movingChecker_{ nullptr };
  mutable House::position targetPosition_;
  mutable int targetIndex_;
  mutable Point target_;
};


#endif // !NARD_ENEMY_HPP
